# MicroPython v1.12 firmwares with ['ulab' module](https://github.com/v923z/micropython-ulab) for [Pyboard v1.1](https://store.micropython.org/product/PYBv1.1)/[Lite v1.0](https://store.micropython.org/product/PYBLITEv1.0)/[D SF2](https://store.micropython.org/product/PYBD-SF2-W4F2)/[D SF3](https://store.micropython.org/product/PYBD-SF3-W4F2)/[D SF6](https://store.micropython.org/product/PYBD-SF6-W4F2)


## ulab

['ulab' MicroPython module](https://github.com/v923z/micropython-ulab) is a numpy-like array manipulation library, written in C (native  module) so it needs to be included in MicroPython/CircuitPython firmware to be used. More about 'ulab' module :  
- [official documentation](https://micropython-ulab.readthedocs.io/);
- [tutorial 'ulab: Crunch Numbers fast in CircuitPython. Use numpy-like commands to process data quickly' from Adafruit Learn](https://learn.adafruit.com/ulab-crunch-numbers-fast-with-circuitpython);
- [topic 'ulab, or what you will - numpy on bare metal' in MicroPython forum](https://forum.micropython.org/viewtopic.php?f=3&t=7005).


## Firmware features

The 14 MicroPython firmwares here are named in the form :  
```<Pyboard name>-<ulab>-<'sp-' or 'dp-'><optional 'thread-' or 'network-'><version with date>.dfu```   
where :
- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html) and allowing multithreading;
- 'network' means Pyboard v1.1/Lite v1.0 firmware with drivers for [CC3000 WiFi modules](https://docs.micropython.org/en/latest/library/network.CC3K.html) and [WIZnet5x00 Ethernet modules](https://docs.micropython.org/en/latest/library/network.WIZNET5K.html).

For example :  
```PYBD-SF6-ulab-dp-thread-v1.12-624-g57333f6ea_20200714.dfu```
means it is a v1.12-624-g57333f6ea firmware, from July 14 2020, with ulab module included, double precision float point numbers and threads enabled, for Pyboard D SF6.

(In 2020-07-14) There was an issue with 'dp' (FP64/double precision float point numbers) on Pyboard's with only native single precision floating point hardware (all except Pyboard D SF6), due to 'ulab' use of 'round' function not implemented in MicroPython builds with double precision not native. See the 'ulab' [issue #132 'Error building/linking ulab with double precision float (FP64/DP)'](https://github.com/v923z/micropython-ulab/issues/132) and MicroPython [pull request #6242 - 'add round.c to libm_dbl'](https://github.com/micropython/micropython/pull/6242). So, **at that moment only Pyboard D SF6 could be built with 'dp' and 'ulab'**.


## (Optional) Building firmware with 'make' options

All MicroPython firmwares here were compiled using :
- 'rcolistete' fork of [MicroPython source code](https://github.com/rcolistete/micropython/), branch 
['pyb_sf2_frozen_2MB'](https://github.com/rcolistete/micropython/tree/pyb_sf2_frozen_2MB) (to allow 'ulab' fit in the small internal flash of Pyboard D SF2/SF3), v1.12-624-g57333f6ea from the commit ['PYB_SF2 : .py frozen modules moved to external flash- 57333f6ea...'](https://github.com/rcolistete/micropython/commit/57333f6ea6660366f5497027b1b9b6905505b9eb), in July 14 2020;
- [ulab source code](https://github.com/v923z/micropython-ulab), v0.51.1 from the [PR#134 - 'fixed incompatibility when enabling '_thread' module from the make command'](https://github.com/v923z/micropython-ulab/pull/134), in July 14 2020;
- gcc-arm cross-compiler ['gcc-arm-none-eabi-8-2019-q3-update-linux.tar.bz2'](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads). Avoid version 10.1 as it is giving link error when compiling with 'ulab' module, so that the firmware is not created.

To build the MicroPython firmware for STM32 boards with 'ulab' module, look at the :  
- [instructions of MicroPython source code for STM32 boards](https://github.com/micropython/micropython/tree/master/ports/stm32);
- [Wiki 'Building Micropython Binaries'](https://github.com/micropython/micropython/wiki/Building-Micropython-Binaries);
- [instructions of 'ulab' repository](https://github.com/v923z/micropython-ulab#compiling).

So the 'make' commands are appended with 'USER_C_MODULES=../../../ulab all' to build 'ulab' module in firmware.

Some of the commands that can be used to build the firmwares listed here, where BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6]. After building, the 'firmware.dfu' is placed at 'stm32/build-$BOARD, which should be copied before the following build.
```
# ulab and sp version (is the default for [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3]) :
[stm32]$ make -j8 BOARD=PYBV11 USER_C_MODULES=../../../ulab all
# ulab and dp version (is the default for PYBD_SF6) :
[stm32]$ make -j8 BOARD=PYBD_SF6 USER_C_MODULES=../../../ulab all
# ulab and sp version for PYBD_SF6 :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=single BOARD=PYBD_SF6 USER_C_MODULES=../../../ulab all
# ulab, sp and thread version, BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3] :
[stm32]$ make -j8 CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBV11 USER_C_MODULES=../../../ulab all
# ulab, dp and thread version for PYBD_SF6 :
[stm32]$ make -j8 CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBD_SF6 USER_C_MODULES=../../../ulab all
# ulab, sp and thread version for PYBD_SF6 :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=single CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBD_SF6 USER_C_MODULES=../../../ulab all
# ulab, sp and network version, BOARD can be [PYBV11 PYBLITEV10] :
[stm32]$ make -j8 MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=PYBV11 USER_C_MODULES=../../../ulab all
```
Loops for building firmwares with 'ulab', 'sp' and with/without 'thread' for 5 Pyboard's :
```
# ulab and sp :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=single BOARD=$BOARD USER_C_MODULES=../../../ulab all; done
# ulab, sp and thread :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=double CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=$BOARD USER_C_MODULES=../../../ulab all; done
```
Loop for building firmwares with 'ulab', 'sp' and 'network' for 2 Pyboard's :
```
[stm32]$ for BOARD in PYBV11 PYBLITEV10; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=single MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=$BOARD USER_C_MODULES=../../../ulab all; done
```
After each building loop, the 'firmware.dfu' placed at 'stm32/build-$BOARD should be copied before the following loop.