# MicroPython com M5Stack Core (Basic/PSRAM MPU9250) :

<img src="https://cdn.shopify.com/s/files/1/0056/7689/2250/products/BASIC_1200x1200.jpg" alt="M5Stack Basic" width="300"/>

- 1x M5Stack Basic, US$35, comprado em 06/12/2017 :  
[M5Stack Basic Docs](https://docs.m5stack.com/#/en/core/basic)  
[Base Core Bottom Docs](https://docs.m5stack.com/#/en/base/core_bottom)  
[M5Stack Basic @ M5Stack Store](https://m5stack.com/collections/m5-core/products/basic-core-iot-development-kit)  
[M5Stack Basic @ M5Stack AliExpress Store](https://www.aliexpress.com/item/32837164440.html)  
ESP32 520KB SRAM, 4MB flash, TFT Colour LCD 2" 320x240 ILI9342C, 3 user buttons, 1 power/reset button, 
uSD (<= 16GB), Core Bottom with 150 mAh battery and many male/female headers, speaker 1W, USB-C (power/serial), Grove connector, black case.  
É modelo antigo que não tem o IP5306 (CI gerenciador de energia).

<img src="https://abra-electronics.com/images/detailed/144/M5stackPSRAM.jpg" alt="M5Stack Gray 4MB PSRAM with MPU9250" width="300"/>

- 1x M5Stack Gray 4MB PSRAM with MPU9250, US$40.85, comprado em 24/01/2018 :  
[Base Core Bottom Docs](https://docs.m5stack.com/#/en/base/core_bottom)  
[M5Stack Gray PSRAM MPU9250 @ Abra-Electronics Store](https://abra-electronics.com/robotics-embedded-electronics/m5stack/m5stack-psram-m5-stack-mpu9250-psram-kit-based-on-esp8266-iot-grey.html)  
Site descontinuado, versão nova 2.0 é "M5Stack NEW PSRAM 2.0 FIRE MIC/BLE SH200Q+BMM150" :  
[M5Stack Basic @ M5Stack AliExpress Store](https://www.aliexpress.com/item/32847906756.html)  
ESP32 520KB SRAM + 4MB PSRAM, 4MB flash, IMU MPU9250, TFT Colour LCD 2" 320x240 ILI9342C, 3 user buttons, 1 power/reset button, 
uSD (<= 16GB), Core Bottom with 150 mAh battery and many male/female headers, speaker 1W, USB-C (power/serial), Grove connector, gray case.  
É modelo antigo que não tem o IP5306 (CI gerenciador de energia), e precisa tirar os 4 ímãs internos da base para não interferir no magnetômetro.

Ligar pressionando o botão vermelho lateral.  
Pressionar 2x o botão vermelho lateral para desligar (sem ter o cabo USB-C conectado, senão só dá um reboot).  
Caso necessário para apagar/gravar firmware, ligar pressionando o 3o botão (C) para entrar em modo 'flash'. 


## Opções de firmware MicroPython para M5Stack Core :

1. UIFlow 1.4.5 é baseado em MicroPython 1.11 (29/05/2019) oficial para ESP32, com grande personalização da M5Stack. Reconhece a tela, tem 
vários drivers para as 'Units', mas não reconhece PSRAM, nem o magnetômetro do MPU9250;
2. M5Cloud offline (antigo, de 05/2018) é baseado em MicroPython_ESP32_psRAM_LoBo (antigo, de 09/2018), com grande personalização da M5Stack. 
Reconhece a tela, PSRAM e MPU9250. Mas tem poucos drivers, sem o módulo Unit do UIFlow;
3. MicroPython ESP32 Lobo (antigo, de 09/2018) + repositório tuupola (03/2019). Reconhece a tela, PSRAM e MPU9250. Mas tem poucos drivers, 
sem o módulo Unit do UIFlow;
4. MicroPython v1.12 (12/2019) ou mais recente oficial. Reconhece PSRAM. Tem que instalar manualmente drivers de terceiros para a tela ILI9342C e 
MPU9250. 



## 1. M5Stack Core (Basic/PSRAM MPU9250) com firmware UIFlow :

UIFlow >= 1.3.2 é baseado em Micropython oficial, no caso UIFlow 1.4.5 usa 
[Micropython v1.11 oficial de 29/05/2019](https://github.com/micropython/micropython/releases/tag/v1.11) com extensas personalizações da M5Stack.  
Reconhece a tela, não reconhece PSRAM, nem o magnetômetro do MPU9250 (somente o acelerômetro e giroscópio ao usar o driver MPU6050).  
Tem vários drivers via [módulo Unit do UIFlow](https://github.com/m5stack/UIFlow-Code/wiki/Unit) que reconhece vários dispositivos em Units, etc.

### 1.1 Instalando firmware :

Via terminal, apagando flash via ['esptool'](https://pypi.org/project/esptool/) :  
`$ pip install esptool`
`$ esptool.py -p /dev/ttyUSB0 -b 115200 --chip esp32 erase_flash`  
e instalando firmware UIFlow, dentro de 'UIFlow-Desktop-IDE_Linux_v1.4.5/assets/firmwares/UIFlow-v1.4.5/UIFlow-Firmware-1.4.5/firmware_en/' :  
`$ ./flash`

### 1.2 Testes e exemplos :

- tem muitos arquivos (imagens na maioria) em '/flash', que podem ser copiados usando ['rshell'](https://github.com/dhylands/rshell) (mais fácil) 
ou ['ampy'](https://github.com/scientifichackers/ampy) :
```
$ rshell -p /dev/ttyUSB0
> ls -l /flash
         0 Feb 24 2050  apps/
         0 Feb 24 2050  blocks/
       992 Feb 24 2050  boot.py
       146 Feb 24 2050  config.json
        49 Feb 24 2050  config.py
       275 Feb 24 2050  debug.py
         0 Feb 24 2050  emojiImg/
      1235 Feb 24 2050  flow.py
       904 Feb 24 2050  flow_usb.py
         0 Feb 24 2050  image_app/
         0 Feb 24 2050  img/
     12691 Feb 24 2050  main.py
         0 Feb 24 2050  res/
```
- dando reset apertando uma vez o botão laterial vermelho :
```
I (32) boot: ESP-IDF v3.3-beta1-696-gc4c54ce07 2nd stage bootloader
I (32) boot: compile time 16:59:35
I (32) boot: Enabling RNG early entropy source...
I (38) boot: SPI Speed      : 40MHz
I (42) boot: SPI Mode       : DIO
I (46) boot: SPI Flash Size : 4MB
I (50) boot: Partition Table:
I (54) boot: ## Label            Usage          Type ST Offset   Length
I (61) boot:  0 nvs              WiFi data        01 02 00009000 00006000
I (68) boot:  1 phy_init         RF data          01 01 0000f000 00001000
I (76) boot:  2 factory          factory app      00 00 00010000 001f0000
I (83) boot:  3 internalfs       Unknown data     01 81 00200000 001ff000
I (91) boot: End of partition table
I (95) esp_image: segment 0: paddr=0x00010020 vaddr=0x3f400020 size=0xe96fc (956156) map
I (435) esp_image: segment 1: paddr=0x000f9724 vaddr=0x3ffbdb60 size=0x034dc ( 13532) load
I (441) esp_image: segment 2: paddr=0x000fcc08 vaddr=0x40080000 size=0x00400 (  1024) load
I (443) esp_image: segment 3: paddr=0x000fd010 vaddr=0x40080400 size=0x03000 ( 12288) load
I (456) esp_image: segment 4: paddr=0x00100018 vaddr=0x400d0018 size=0xdbea8 (900776) map
I (772) esp_image: segment 5: paddr=0x001dbec8 vaddr=0x40083400 size=0x12be8 ( 76776) load
I (802) esp_image: segment 6: paddr=0x001eeab8 vaddr=0x400c0000 size=0x00064 (   100) load
I (803) esp_image: segment 7: paddr=0x001eeb24 vaddr=0x50000000 size=0x00808 (  2056) load
I (823) boot: Loaded app from partition at offset 0x10000
I (823) boot: Disabling RNG early entropy source...
I (824) cpu_start: Pro cpu up.
I (827) cpu_start: Application information:
I (832) cpu_start: Compile time:     Feb 25 2020 16:59:41
I (838) cpu_start: ELF file SHA256:  0000000000000000...
I (844) cpu_start: ESP-IDF:          v3.3-beta1-696-gc4c54ce07
I (851) cpu_start: Starting app cpu, entry point is 0x400836e4
I (0) cpu_start: App cpu up.
I (861) heap_init: Initializing. RAM available for dynamic allocation:
I (868) heap_init: At 3FFAE6E0 len 0000F480 (61 KiB): DRAM
I (874) heap_init: At 3FFCB2C8 len 00014D38 (83 KiB): DRAM
I (880) heap_init: At 3FFE0440 len 00003AE0 (14 KiB): D/IRAM
I (887) heap_init: At 3FFE4350 len 0001BCB0 (111 KiB): D/IRAM
I (893) heap_init: At 40095FE8 len 0000A018 (40 KiB): IRAM
I (899) cpu_start: Pro cpu start user code
I (23) cpu_start: Starting scheduler on PRO CPU.
I (0) cpu_start: Starting scheduler on APP CPU.

Internal FS (FatFS): Mounted on partition 'internalfs' [size: 2093056; Flash address: 0x200000]
----------------
Filesystem size: 2031616 B
           Used: 1380352 B
           Free: 651264 B
----------------
I (501) [TFTSPI]: attached display device, speed=8000000
I (502) [TFTSPI]: bus uses native pins: false
W (711) sdspi_host: spi bus changed (1 -> 2)
[ M5 ] init sd card Fail
[ M5 ] node id:30aea44f9d18, api key:B8854284
MicroPython v1.11-312-g944acf81d-dirty on 2020-02-25; ESP32 module with ESP32
Type "help()" for more information.
>>>
```

- com 'boot.py' e 'main.py' de 'UIFlow_v1.45 - Base Micropython v1.11' :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.11-312-g944acf81d-dirty on 2020-02-25; ESP32 module with ESP32  
import gc
gc.collect()
gc.mem_free()
   77264
```

- apagando 'main.py' e usando 'boot.py' simplificado :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.11-312-g944acf81d-dirty on 2020-02-25; ESP32 module with ESP32
import gc
gc.collect()
gc.mem_free()  
  77024
```

Com 'boot.py' mínimo (só 2 primeiras linhas), sem inicializar gráficos, mas REPL fica difícil de ser acessado :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.11-312-g944acf81d-dirty on 2020-02-25; ESP32 module with ESP32  
import gc
gc.collect()
gc.mem_free()
   85424
```

- ver versão de MicroPython (após boot/reset também mostra) :
```
import os
os.uname()
    (sysname='esp32', nodename='esp32', release='1.11.0', version='v1.11-312-g944acf81d-dirty on 2020-02-25', machine='ESP32 module with ESP32')
```

- [módulo 'machine'](https://docs.micropython.org/en/latest/library/machine.html) e frequência do ESP32 (240 MHz, não dá para alterar) :
```
import machine
machine.freq()
    240000000
```

- help :
```
help()
    Welcome to MicroPython on the ESP32!

    For generic online docs please visit http://docs.micropython.org/

    For access to the hardware use the 'machine' module:

    import machine
    pin12 = machine.Pin(12, machine.Pin.OUT)
    pin12.value(1)
    pin13 = machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP)
    print(pin13.value())
    i2c = machine.I2C(scl=machine.Pin(21), sda=machine.Pin(22))
    i2c.scan()
    i2c.writeto(addr, b'1234')
    i2c.readfrom(addr, 4)

    Basic WiFi configuration:

    import network
    sta_if = network.WLAN(network.STA_IF); sta_if.active(True)
    sta_if.scan()                             # Scan for available access points
    sta_if.connect("<AP_name>", "<password>") # Connect to an AP
    sta_if.isconnected()                      # Check for successful connection

    Control commands:
    CTRL-A        -- on a blank line, enter raw REPL mode
    CTRL-B        -- on a blank line, enter normal REPL mode
    CTRL-C        -- interrupt a running program
    CTRL-D        -- on a blank line, do a soft reset of the board
    CTRL-E        -- on a blank line, enter paste mode

    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (drivers, etc, do UIFlow MicroPython firmware) :

```
help('modules')
    __main__          flowlib/lib/time_ex                 flowlib/units/_earth                mlx90640
    _onewire          flowlib/lib/urequests               flowlib/units/_env                  neopixel
    _thread           flowlib/lib/wave  flowlib/units/_ext_io               network
    array             flowlib/lib/wavplayer               flowlib/units/_finger               os
    binascii          flowlib/lib/wifiCardKB              flowlib/units/_gps                  random
    btree             flowlib/lib/wifiCfg                 flowlib/units/_ir re
    builtins          flowlib/lib/wifiWebCfg              flowlib/units/_joystick             select
    cmath             flowlib/m5cloud   flowlib/units/_light                socket
    collections       flowlib/m5mqtt    flowlib/units/_makey                ssl
    display           flowlib/m5stack   flowlib/units/_mlx90640             struct
    errno             flowlib/m5ucloud  flowlib/units/_ncir                 sys
    esp               flowlib/module    flowlib/units/_pahub                time
    esp32             flowlib/modules/_baseX              flowlib/units/_pbhub                ubinascii
    espnow            flowlib/modules/_cellular           flowlib/units/_pir                  ucollections
    flowlib/app_manage                  flowlib/modules/_goPlus             flowlib/units/_relay                ucryptolib
    flowlib/button    flowlib/modules/_lego               flowlib/units/_rfid                 uctypes
    flowlib/face      flowlib/modules/_legoBoard          flowlib/units/_rgb                  uerrno
    flowlib/faces/_calc                 flowlib/modules/_lidarBot           flowlib/units/_rgb_multi            uhashlib
    flowlib/faces/_encode               flowlib/modules/_lorawan            flowlib/units/_servo                uhashlib
    flowlib/faces/_finger               flowlib/modules/_m5bala             flowlib/units/_tof                  uheapq
    flowlib/faces/_gameboy              flowlib/modules/_plus               flowlib/units/_tracker              uio
    flowlib/faces/_joystick             flowlib/modules/_pm25               flowlib/units/_weight               ujson
    flowlib/faces/_keyboard             flowlib/modules/_servo              flowlib/utils     uos
    flowlib/faces/_rfid                 flowlib/modules/_stepMotor          gc                upip
    flowlib/flowSetup flowlib/peripheral                  hashlib           upip_utarfile
    flowlib/i2c_bus   flowlib/power     heapq             urandom
    flowlib/lib/bmm150                  flowlib/remote    io                ure
    flowlib/lib/bmp280                  flowlib/simple    json              uselect
    flowlib/lib/chunk flowlib/timeSchedule                lidar             usocket
    flowlib/lib/dht12 flowlib/uiflow    logging           ussl
    flowlib/lib/easyIO                  flowlib/unit      m5base            ustruct
    flowlib/lib/emoji flowlib/units/_accel                m5uart            utime
    flowlib/lib/imu   flowlib/units/_adc                  m5ui              utimeq
    flowlib/lib/mpu6050                 flowlib/units/_angle                machine           uwebsocket
    flowlib/lib/mstate                  flowlib/units/_button               math              uzlib
    flowlib/lib/numbers                 flowlib/units/_cardKB               microWebSocket    zlib
    flowlib/lib/pid   flowlib/units/_color                microWebSrv
    flowlib/lib/sh200q                  flowlib/units/_dac                  microWebTemplate
    flowlib/lib/speak flowlib/units/_dual_button          micropython
    Plus any modules on the filesystem
```

- [módulo 'esp'](https://docs.micropython.org/en/latest/library/esp.html) e tamanho da memória flash interna (4 MB) :
```
import esp
esp.
    __class__       __name__        LOG_DEBUG       LOG_ERROR
    LOG_INFO        LOG_NONE        LOG_VERBOSE     LOG_WARNING
    dht_readinto    flash_erase     flash_read      flash_size
    flash_user_start                flash_write     gpio_matrix_in
    gpio_matrix_out                 neopixel_write  osdebug
esp.flash_size()
    4194304   # 4MB
```

- [módulo 'esp32'](https://docs.micropython.org/en/latest/library/esp32.html), temperatura e leitura do sensor Hall do ESP32 :
```
import esp32
esp32.
    __class__       __name__        ULP             WAKEUP_ALL_LOW
    WAKEUP_ANY_HIGH                 hall_sensor     raw_temperature
    wake_on_ext0    wake_on_ext1    wake_on_touch
esp32.raw_temperature()   # in F
    128
(esp32.raw_temperature() - 32)/1.8   # in C
    53.33333
esp32.hall_sensor()
    87
```

- módulo 'm5stack' :
```
import m5stack
m5stack.
    __class__       __name__        const           __file__
    binascii        display         m5base          machine
    os              power           __VERSION__     apikey
    node_id         btnA            btnC            lcd
    btnText         BtnChild        Btn             btn
    timEx           hwDeinit        timerSch        btnB
    Speaker         rgb             Rgb_multi       IP5306
    speaker         _sdCardState    sd_mount        sd_umount
    get_sd_state    time_ex         timeSchedule
```

- [módulo 'm5ui'](https://github.com/m5stack/UIFlow-Code/wiki/M5UI) :
```
import m5ui
m5ui.
    __class__       M5Button        M5Circle        M5Img
    M5Rect          M5TextBox       M5Title         M5UI_Deinit
    setScreenColor
```

- módulo 'uiflow' :
```
import uiflow
uiflow.
    __class__       __name__        const           start
    __file__        binascii        display         gc
    m5base          machine         os              power
    time            wait            apikey          serverChoose
    node_id         loopExit        loopSetIdle     loopState
    btnA            btnC            lcd             btnText
    flowDeinit      flowExit        BtnChild        Btn
    btn             wait_ms         timEx           config_normal
    cfgWrite        cfgRead         modeSet         core_start
    resetDefault    startBeep       _p2pData        _nextP2PTime
    sendP2PData     setP2PData      getP2PData      _exitState
    _is_remote      remoteInit      hwDeinit        timerSch
    btnB            Speaker         rgb             Rgb_multi
    IP5306          speaker         sd_mount        sd_umount
    get_sd_state    time_ex         timeSchedule
```

- [módulo 'm5stack"](https://github.com/m5stack/UIFlow-Code/wiki/Graphic) com gráficos exibindo texto, linhas e imagem na tela TFT de 320x240 pixels :
```
import gc, os
from m5stack import lcd
lcd.
    __class__       clear           print           BLACK
    BLUE            BMP             BOTTOM          CENTER
    COLOR_BITS16    COLOR_BITS24    CYAN            DARKCYAN
    DARKGREEN       DARKGREY        FONT_7seg       FONT_Comic
    FONT_Default    FONT_DefaultSmall               FONT_DejaVu18
    FONT_DejaVu24   FONT_DejaVu40   FONT_DejaVu56   FONT_DejaVu72
    FONT_Minya      FONT_Small      FONT_Tooney     FONT_Ubuntu
    GREEN           GREENYELLOW     HSPI            JPG
    LANDSCAPE       LANDSCAPE_FLIP  LASTX           LASTY
    LIGHTGREY       M5STACK         MAGENTA         MAROON
    NAVY            OLIVE           ORANGE          PINK
    PORTRAIT        PORTRAIT_FLIP   PURPLE          RED
    RIGHT           VSPI            WHITE           YELLOW
    arc             attrib7seg      backlight       circle
    clearwin        compileFont     deinit          drawCircle
    drawLine        drawPixel       drawRect        drawRoundRect
    drawTriangle    ellipse         fill            fillCircle
    fillRect        fillRoundRect   fillScreen      fillTriangle
    font            fontSize        getCursor       get_bg
    get_fg          hsb2rgb         image           init
    line            lineByAngle     orient          pixel
    polygon         println         qrcode          rect
    resetwin        restorewin      roundrect       savewin
    screensize      setBrightness   setColor        setCursor
    setRotation     setTextColor    set_bg          set_fg
    setwin          text            textClear       textWidth
    text_x          text_y          tft_deselect    tft_readcmd
    tft_select      tft_setspeed    tft_writecmd    tft_writecmddata
    triangle        winsize
lcd.clear(0x000040)   # blue
lcd.font(lcd.FONT_Ubuntu, color=0xFFFFFF)
lcd.print("MicroPython " + os.uname().version)
gc.collect()
lcd.print("\nFree RAM = " + str(gc.mem_free()) + ' bytes')
#
lcd.clearwin()
for y in range(0, 239, 5):
    lcd.line(0, y, 319, 239, 0xFF0000)
for y in range(0, 239, 5):
    lcd.line(0, 0, 319, y, 0x0000FF)
#
lcd.image(0,0,"img/m5.jpg")
```

- [módulo 'display'](https://github.com/m5stack/UIFlow-Code/wiki/Display) com funções de mais baixo nível, usando 'boot.py' 
sem inicializar tela (i.e., sem `from m5stack import lcd`) :
```
import display
tft = display.TFT()
tft.
    __class__       clear           print           BLACK
    BLUE            BMP             BOTTOM          CENTER
    COLOR_BITS16    COLOR_BITS24    CYAN            DARKCYAN
    DARKGREEN       DARKGREY        FONT_7seg       FONT_Comic
    FONT_Default    FONT_DefaultSmall               FONT_DejaVu18
    FONT_DejaVu24   FONT_DejaVu40   FONT_DejaVu56   FONT_DejaVu72
    FONT_Minya      FONT_Small      FONT_Tooney     FONT_Ubuntu
    GREEN           GREENYELLOW     HSPI            JPG
    LANDSCAPE       LANDSCAPE_FLIP  LASTX           LASTY
    LIGHTGREY       M5STACK         MAGENTA         MAROON
    NAVY            OLIVE           ORANGE          PINK
    PORTRAIT        PORTRAIT_FLIP   PURPLE          RED
    RIGHT           VSPI            WHITE           YELLOW
    arc             attrib7seg      backlight       circle
    clearwin        compileFont     deinit          drawCircle
    drawLine        drawPixel       drawRect        drawRoundRect
    drawTriangle    ellipse         fill            fillCircle
    fillRect        fillRoundRect   fillScreen      fillTriangle
    font            fontSize        getCursor       get_bg
    get_fg          hsb2rgb         image           init
    line            lineByAngle     orient          pixel
    polygon         println         qrcode          rect
    resetwin        restorewin      roundrect       savewin
    screensize      setBrightness   setColor        setCursor
    setRotation     setTextColor    set_bg          set_fg
    setwin          text            textClear       textWidth
    text_x          text_y          tft_deselect    tft_readcmd
    tft_select      tft_setspeed    tft_writecmd    tft_writecmddata
    triangle        winsize
tft.init(tft.M5STACK, width=240, height=320, rst_pin=33, backl_pin=32, miso=19, mosi=23, clk=18, cs=14, dc=27, bgr=True, backl_on=1)
tft.clear(0xFF0000)   # fundo vermelho
tft.screensize()
    (320, 240)
```

- [botões A, B e C](https://github.com/m5stack/UIFlow-Code/wiki/Event#button) :
```
from m5stack import btnA, btnB, btnC
btnA.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          pin             wasPressed
    wasDoublePress  pressFor        wasReleased     isPressed
    isReleased      restart         upDate          _event
    _eventLast      _valueLast      _pressTime      _releaseTime
    _doubleTime     _dbtime         _holdTime       _cbState
    _eventTime      cb
btnA.isPressed()    # sem pressionar o 1o botão frontal (abaixo da tela), botão A
    False
btnA.isPressed()    # pressionando o 1o botão frontal (abaixo da tela), botão A 
    True
btnA.wasPressed(lambda: print('button A pressed'))               # ativa interrupção ao pressionar o botão A, mostrando texto no terminal
btnB.wasReleased(lambda: print('button B released'))             # ativa interrupção ao liberar o botão B, mostrando texto no terminal
btnC.wasDoublePress(lambda: print('button C double released'))   # ativa interrupção ao pressionar duas vezes o botão C, mostrando texto no terminal
```

- [sub-módulo 'm5stack.speaker'](https://github.com/m5stack/UIFlow-Code/wiki/Hardware#speak) ('speak' mudou de nome para 'speaker') :
```
from m5stack import speaker
speaker.
    __class__       __init__        __module__      __qualname__
    __dict__        pin             _timer          checkInit
    _timeout_cb     tone            sing            setBeat
    setVolume       pwm_init        _volume         _beat_time
speaker.setVolume(1)     # default = 2
speaker.tone(600, 500)   # f = 500 Hz, T = 500 ms
speaker.sing(600, 1)     # f = 500 Hz, 1 beat = 500 ms
```

- [módulo 'i2c_bus'](https://github.com/m5stack/UIFlow-Code/wiki/Advanced#i2c) de UIFlow, com barramento I2C listando o sensor MPU9250 :
```
import i2c_bus
i2c = i2c_bus.get(i2c_bus.M_BUS)
i2c.scan()
    []        # on M5Stack Basic
    [104]     # on M5Stack Gray 4MB PSRAM with MPU9250 the default I2C address of MPU9250 is 0x68 = 104
```

- se m5stack não foi carregado (i. e., I2C não está sendo usado), então se pode inicializar I2C via 
['machine.I2C'](https://docs.micropython.org/en/latest/esp32/quickref.html#i2c-bus), forma padrão do MicroPython para ESP32 :
```
from machine import I2C
i2c = I2C(freq=400000, sda=21, scl=22)
i2c.scan()
    []        # on M5Stack Basic
    [104]     # on M5Stack Gray 4MB PSRAM with MPU9250 the default I2C address of MPU9250 is 0x68 = 104
```

- no 'M5Stack Gray 4MB PSRAM with MPU9250' (accelerometer + gyroscope + magnetometer). UIFlow recente não tem driver MPU9250, só 
[driver MPU6050 (accelerometer + gyroscope)](https://github.com/m5stack/UIFlow-Code/wiki/Hardware#mpu6050)
```
import mpu6050
imu = mpu6050.MPU6050()
imu.
    __class__       __enter__       __exit__        __init__
    __module__      __qualname__    __dict__        address
    i2c             whoami          setGyroOffsets  angleX
    ypr             acceleration    gyro            _accel_fs
    _gyro_fs        preInterval     accCoef         gyroCoef
    angleGyroX      angleGyroY      angleGyroZ      angleZ
    angleY          gyroXoffset     gyroYoffset     gyroZoffset
    _accel_so       _gyro_so        _register_short
    _register_three_shorts          _register_char  _accel_sf
    _gyro_sf
imu.acceleration
   (-0.01, 1.03, -0.028)  # M5Stack na vertical
imu.gyro
   (2.641, 0.489, -0.504)
```

<img src="https://docs.m5stack.com/assets/img/product_pics/unit/tof/unit_tof_01.jpg" alt="M5Stack Unit ToF VL53L0X " width="300"/>

- [module TOF VL53L0X](https://github.com/m5stack/UIFlow-Code/wiki/Unit#tof). 
[Código-fonte de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_tof.py) mostra 70 atualizações/leituras por segundo.
```
import i2c_bus
i2c = i2c_bus.get(i2c_bus.M_BUS)
i2c.scan()
    [41, 104]   # ADS1100 default I2C address 0x29 = 41
import time
import unit
unit_tof = unit.get(unit.TOF, unit.PORTA)   # erro 'Unit: TOF unit maybe not connect' com M5Stack Gray PSRAM MPU9250...
unit_tof.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          distance        timer
    state           i2c             _available      _register_short
    _register_char  _update
while True:
    print(unit_tof.distance)
    time.sleep_ms(200)
```

<img src="https://docs.m5stack.com/assets/img/product_pics/unit/M5GO_Unit_adc.png" alt="M5Stack Unit ADC 16 bits I2C ADS1100 " width="300"/>

- [module ADC 16 bits I2C ADS1100](https://github.com/m5stack/UIFlow-Code/wiki/Unit#adc). 
[Código-fonte de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_adc.py), onde se vê que as 
opções (mode, rate, gain, etc) não estão implementadas nesse código-fonte.  
Default : offset = 0.25V, mode = continue (or single shot), rate = 15 samples/s (or 30/60/240), gain = 1x (or 2x/4x/8x).
```
import i2c_bus
i2c = i2c_bus.get(i2c_bus.M_BUS)
i2c.scan()
    [72, 104]   # ADS1100 default I2C address 0x48 = 72
import unit
unit_adc = unit.get(unit.ADC, unit.PORTA)
unit_adc.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          mode            offset
    rate            i2c             _available      _write_u8
    _read_u16       measure_set     voltage         gain
    mini_code       gain_code
unit_adc.
unit_adc.voltage   # get voltage, value range 0~12V
    3.196          # 3V3 of M5Stack Gray
```


### 1.3 Consumo de energia

#### 1.3.1 Medição de energia do 'M5Stack Basic' usando UM25C, sem M5Stack Core Bottom (pois pode estar carregando a bateria) :
- com 'boot.py' e 'main.py' originais, ou com 'boot.py' simplificado, i = 100 mA;
- 100 mA -> 53 mA com 'lcd.setBrightness(0)'' (tela em modo de economia de energia) :
```
from m5stack import lcd
lcd.setBrightness(0)   # 0-100
```
- 100 mA -> 20 mA com 'lcd.setBrightness(0)'' e 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
from m5stack import lcd
lcd.setBrightness(0)   # 0-100
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```
- 100 mA -> 20 mA com 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```

#### 1.3.2 Medição de energia do 'M5Stack Gray 4MB PSRAM with MPU9250' usando UM25C, sem M5Stack Core Bottom (pois pode estar carregando a bateria) :
- com 'boot.py' e 'main.py' originais, ou com 'boot.py' simplificado, i = 91 mA;
- 91 mA -> 56 mA com 'lcd.setBrightness(0)'' (tela em modo de economia de energia) :
```
from m5stack import lcd
lcd.setBrightness(0)   # 0-100
```
- 91 mA -> 23 mA com 'lcd.setBrightness(0)'' e 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
from m5stack import lcd
lcd.setBrightness(0)   # 0-100
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```
- 91 mA -> 23 mA com 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```



## 2. M5Stack Core (Basic/PSRAM MPU9250) com firmware M5Cloud offline :

Vide [repositório do firmware 'M5Stack Cloud'](https://github.com/m5stack/M5Cloud), com breve [documentação na página principal]
(https://github.com/m5stack/M5Cloud/blob/master/README.md) e [vários exemplos](https://github.com/m5stack/M5Cloud/tree/master/examples).

Reconhece a tela, tem firmware que reconhece PSRAM, tem driver MPU9250, mas é firmware M5Stack antigo, de 05/2018, baseado no 
firmware [MicroPython_ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki), que também é antigo (09/2018).  
Tem poucos drivers (ssd1306, ak8963, mpu6500, mpu9250), sem o [módulo Unit do UIFlow](https://github.com/m5stack/UIFlow-Code/wiki/Unit) 
que reconhece vários dispositivos em Units, etc.


### 2.1 Instalando firmware :

Apagando flash, ligar pressionando o 3o botão (C) para entrar em modo 'flash' :  
`$ pip install esptool`
`$ esptool.py -p /dev/ttyUSB0 -b 115200 --chip esp32 erase_flash`  
Instalando firmware M5Cloud offline via terminal, usando repositório 
['M5Cloud-master/firmwares/OFF-LINE/'](https://github.com/m5stack/M5Cloud/tree/master/firmwares/OFF-LINE), para M5Stack Basic, sem PSRAM :  
`$ esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash --flash_mode dio -z 0x1000 m5stack-20180516-v0.4.0.bin`  
para M5Stack Gray, com PSRAM :  
`$ esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash --flash_mode dio -z 0x1000 m5stack-psram-20180516-v0.4.0.bin`

### 2.2 Testes e exemplos :

- tem poucos arquivos em '/flash', que podem ser copiados usando 'rshell' (mais fácil) ou 'ampy' :
```
$ rshell -p /dev/ttyUSB0
> ls -l /flash
   363 Dec 31 1969  boot.py
   114 Dec 31 1969  main.py
```

- dando reset apertando uma vez o botão lateral vermelho ou via 'Ctrl+D' no REPL no M5Stack Basic sem PSRAM :
```
Internal FS (SPIFFS): Mounted on partition 'internalfs' [size: 2424832; Flash address: 0x1B0000]
----------------
Filesystem size: 2221568 B
           Used: 1280 B
           Free: 2220288 B
----------------

Device ID:30aea44fb040

FreeRTOS running on BOTH CORES, MicroPython task started on App Core (1).

 Reset reason: Power on reset
    uPY stack: 19456 bytes
     uPY heap: 80000/10144/69856 bytes

MicroPython ESP32_LoBo_v3.2.16 - 2018-05-15 on M5Stack with ESP32
Type "help()" for more information.
```
no M5Stack Gray com PSRAM :
```
Internal FS (SPIFFS): Mounted on partition 'internalfs' [size: 2293760; Flash address: 0x1D0000]
----------------
Filesystem size: 2100992 B
           Used: 768 B
           Free: 2100224 B
----------------

Device ID:30aea44f9d18

FreeRTOS running on BOTH CORES, MicroPython task started on App Core (1).

 Reset reason: Power on reset
    uPY stack: 19456 bytes
     uPY heap: 3073664/10128/3063536 bytes (in SPIRAM using heap_caps_malloc)

MicroPython ESP32_LoBo_v3.2.16 - 2018-05-15 on M5Stack with ESP32
Type "help()" for more information.
>>>
```

- com 'boot.py' e 'main.py' de M5Stack Cloud offline (MicroPython ESP32_LoBo_v3.2.16), no M5Stack Basic sem PSRAM :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython ESP32_LoBo_v3.2.16 - 2018-05-15 on M5Stack with ESP32
import gc
gc.collect()
gc.mem_free()
    70128   # 68.5 kB
```
no M5Stack Gray com PSRAM :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython ESP32_LoBo_v3.2.16 - 2018-05-15 on M5Stack with ESP32
import gc
gc.collect()
gc.mem_free()
    3063760   # 2.992 kB
```

- apagando 'main.py' e usando 'boot.py' simplificado, no M5Stack Basic sem PSRAM :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython ESP32_LoBo_v3.2.16 - 2018-05-15 on M5Stack with ESP32
import gc
gc.collect()
gc.mem_free()
    70144   # 68.5 kB
```
Com 'boot.py' mínimo (só 2 primeiras linhas), sem inicializar gráficos explicitamente (implicitamente o firmware 
imprime 2 linhas na tela, logo 'm5stack.lcd' é sim inicializado no firmware) :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython ESP32_LoBo_v3.2.16 - 2018-05-15 on M5Stack with ESP32
import gc
gc.collect()
gc.mem_free()
    70160   # 68.5 kB
```
no M5Stack Gray com PSRAM :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython ESP32_LoBo_v3.2.16 - 2018-05-15 on M5Stack with ESP32
import gc
gc.collect()
gc.mem_free()  
    3063808   # 2.992 kB
```
Com 'boot.py' mínimo (só 2 primeiras linhas), sem inicializar gráficos explicitamente (implicitamente o firmware 
imprime 2 linhas na tela, logo 'm5stack.lcd' é sim inicializado no firmware) :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython ESP32_LoBo_v3.2.16 - 2018-05-15 on M5Stack with ESP32
import gc
gc.collect()
gc.mem_free()  
    3063824   # 2.992 kB
```

- ver versão de MicroPython (após boot/reset também mostra) :
```
import os
os.uname()
    (sysname='esp32_LoBo', nodename='esp32_LoBo', release='3.2.16', version='ESP32_LoBo_v3.2.16 on 2018-05-15', machine='M5Stack with ESP32')
```

- [módulo 'machine'](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/machine#machinefreqnew_freq) e frequência do ESP32 
(default é 240 MHz, pode ser alterada para 2MHz, 80Mhz, 160MHz, 240MHz, mas com 2MHz o REPL trava) :
```
import machine
machine.freq()
    240000000
machine.freq(80)   # 80 MHz
```

- help :
```
help()
    Welcome to LoBo MicroPython for the ESP32

    For online documentation please visit the Wiki pages:
    https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki

    Based on official MicroPython, this port brings many new features:

    - support for two cores and 4MB SPIRAM (psRAM)
    - improved 'network' module
    - greatly improved thread support
    - support for 3 different internal file systems on top of ESP32 VFS
    - ESP32 native support for SD Card
    - built-in FTP & Telnet servers
    - support for OTA updates
    - many new and improved hardware access modules implemented in C
    and many more...

    Control commands:
    CTRL-A        -- on a blank line, enter raw REPL mode
    CTRL-B        -- on a blank line, enter normal REPL mode
    CTRL-C        -- interrupt a running program
    CTRL-D        -- on a blank line, do a soft reset of the board
    CTRL-E        -- on a blank line, enter paste mode

    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (drivers, etc, do M5Cloud MicroPython firmware) :
```
help('modules')
    __main__          heapq             random            upip_utarfile
    _boot             inisetup          re                upysh
    _thread           io                select            urandom
    ak8963            json              socket            ure
    array             logging           ssd1306           urequests
    binascii          m5cloud           ssl               uselect
    btree             m5stack           struct            usocket
    builtins          machine           sys               ussl
    chunk             math              time              ustruct
    cmath             microWebSocket    tpcalib           utils
    collections       microWebSrv       ubinascii         utime
    curl              microWebTemplate  ucollections      utimeq
    display           micropython       uctypes           uzlib
    errno             modREPL           uerrno            wave
    framebuf          mpu6500           uhashlib          websocket
    freesans20        mpu9250           uheapq            wificonfig
    functools         mtftp             uio               wifisetup
    gc                network           ujson             writer
    gsm               os                uos               ymodem
    hashlib           pye               upip              zlib
    Plus any modules on the filesystem
```

- módulo 'm5stack' :
```
import m5stack
m5stack.
    __class__       __name__        const           machine
    os              time            ubinascii       utils
    lcd             node_id         VERSION         Button
    Speaker         fimage          delay           buttonA
    buttonB         buttonC         speaker
```

- [módulo 'm5stack'](https://github.com/m5stack/M5Cloud#lcd) com gráficos exibindo texto e linhas na tela TFT de 320x240 pixels :
```
import gc, os
from m5stack import lcd
lcd.
    __class__       BLACK           BLUE            BMP
    BOTTOM          CENTER          COLOR_BITS16    COLOR_BITS24
    CYAN            DARKCYAN        DARKGREEN       DARKGREY
    FONT_7seg       FONT_Comic      FONT_Default
    FONT_DefaultSmall               FONT_DejaVu18   FONT_DejaVu24
    FONT_Minya      FONT_Small      FONT_Tooney     FONT_Ubuntu
    GREEN           GREENYELLOW     HSPI            JPG
    LANDSCAPE       LANDSCAPE_FLIP  LASTX           LASTY
    LIGHTGREY       M5STACK         MAGENTA         MAROON
    NAVY            OLIVE           ORANGE          PINK
    PORTRAIT        PORTRAIT_FLIP   PURPLE          RED
    RIGHT           VSPI            WHITE           YELLOW
    arc             attrib7seg      circle          clear
    clearwin        compileFont     deinit          ellipse
    font            fontSize        getCursor       get_bg
    get_fg          hsb2rgb         image           init
    line            lineByAngle     orient          pixel
    polygon         print           println         rect
    resetwin        restorewin      roundrect       savewin
    screensize      setBrightness   setColor        setCursor
    setTextColor    set_bg          set_fg          setwin
    text            textClear       textWidth       text_x
    text_y          tft_deselect    tft_readcmd     tft_select
    tft_setspeed    tft_writecmd    tft_writecmddata
    triangle        winsize
lcd.clear(0x000040)   # blue
lcd.font(lcd.FONT_Ubuntu, color=0xFFFFFF)
lcd.print("MicroPython " + os.uname().version)
gc.collect()
lcd.print("\nFree RAM = " + str(gc.mem_free()) + ' bytes')
#
lcd.clearwin()
for y in range(0, 239, 5):
    lcd.line(0, y, 319, 239, 0xFF0000)
for y in range(0, 239, 5):
    lcd.line(0, 0, 319, y, 0x0000FF)
#
lcd.image(0,0,"img/m5.jpg")   # Copied this image from UIFlow firmware '/flash/img/m5.jpg'
```

- [botões A, B e C](https://github.com/m5stack/M5Cloud#button), notar os nomes diferentes dos 
botões ('buttonA' em M5Cloud -> 'btnA' em UIFlow) :
```
from m5stack import buttonA, buttonB, buttonC
buttonA.
    __class__       __dict__        __init__        __module__
    __qualname__    read            irq_cb          isPressed
    isReleased      wasPressed      wasReleased     pressedFor
    releasedFor     _pin            _wasPressed_cb  _wasReleased_cb
    _releasedFor_cb                 _timeshoot      _dbtime
    _lastState      _startTicks     _timeout        _event
buttonA.isPressed()    # sem pressionar o 1o botão frontal (abaixo da tela), botão A
    False
buttonA.isPressed()    # pressionando o 1o botão frontal (abaixo da tela), botão A 
    True
buttonA.wasPressed(lambda: print('button A pressed'))                  # ativa interrupção ao pressionar o botão A, mostrando texto no terminal
buttonB.wasReleased(lambda: print('button B released'))                # ativa interrupção ao liberar o botão B, mostrando texto no terminal
buttonnC.releasedFor(0.5, lambda: print('button C pressed for 0.5s'))   # ativa interrupção ao pressionar o botão C por >= 0.5s, mostrando texto no terminal
```

- [sub-módulo 'm5stack.speaker'](https://github.com/m5stack/M5Cloud#speaker) :
```
from m5stack import speaker
speaker.
    __class__       __dict__        __init__        __module__
    __qualname__    setblocking     volume          _timeout_cb
    tone            pwm             _timer          _volume
    _blocking
speaker.volume(1)        # default parece ser 2
speaker.tone(600, 500)   # f = 500 Hz, T = 500 ms
```

- inicializa I2C via ['machine.I2C'](https://github.com/m5stack/M5Cloud#i2c), forma padrão do MicroPython para ESP32 :
```
from machine import I2C
i2c = I2C(freq=400000, sda=21, scl=22)
i2c.scan()
    []        # on M5Stack Basic
    [104]     # on M5Stack Gray 4MB PSRAM with MPU9250 the default I2C address of MPU9250 is 0x68 = 104
```

- no 'M5Stack Gray 4MB PSRAM with MPU9250' (accelerometer + gyroscope + magnetometer). M5Cloud/ESP32 Lobo MicroPython firmware tem 
[driver MPU9250](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/blob/master/MicroPython_BUILD/components/micropython/esp32/modules/mpu9250.py),
que é v0.2.0-dev do ['MicroPython MPU-9250 (MPU-6500 + AK8963) I2C driver' do tuupola, atualmente na v0.2.1](https://github.com/tuupola/micropython-mpu9250) :
```
from machine import I2C
import mpu9250
i2c = I2C(freq=400000, sda=21, scl=22)
imu = mpu9250.MPU9250(i2c)
imu.
    __class__       __dict__        __enter__       __exit__
    __init__        __module__      __qualname__    magnetic
    whoami          acceleration    gyro            mpu6500
    ak8963
imu.whoami
    113
imu.acceleration
    (-0.0885854614257813, 10.1562034423828, 0.126892687988281)     # m/s^2, M5Stack Gray na vertical
    (-0.0981622680664063, -0.0598550415039063, 9.73721815185547)   # m/s^2, M5Stack Gray na horizontal
imu.gyro
    (174.948945278021, 33.6776719660191, -59.0452690313322)   # valores sem sentido, ficam oscilando muito, deve ser bug no driver
imu.magnetic
    (-8.490234375, -161.314453125, 78.73125)   # uT, M5Stack Gray na vertical
    (-38.759765625, -167.220703125, 87.6375)   # uT, M5Stack Gray na horizontal, Bx mostra uns -30 uT a mais devido ao geomagnetismo
```

### 2.3 Consumo de energia

#### 2.3.1 Medição de energia do 'M5Stack Basic' usando UM25C, sem M5Stack Core Bottom (pois pode estar carregando a bateria) :
- com 'boot.py' e 'main.py' originais, ou com 'boot.py' simplificado, i = 86 mA;
- 86 mA -> 52 mA com 'lcd.setBrightness(0)'' (tela em modo de economia de energia) :
```
from m5stack import lcd
lcd.setBrightness(0)   # 0-100
```
- 86 mA -> 21 mA com 'lcd.setBrightness(0)'' e 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
from m5stack import lcd
lcd.setBrightness(0)   # 0-100
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```
- 86 mA -> 21 mA com 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```

#### 2.3.2 Medição de energia do 'M5Stack Gray 4MB PSRAM with MPU9250' usando UM25C, sem M5Stack Core Bottom (pois pode estar carregando a bateria) :
- com 'boot.py' e 'main.py' originais, ou com 'boot.py' simplificado, i = 78 mA;
- 78 mA -> 53 mA com 'lcd.setBrightness(0)'' (tela em modo de economia de energia) :
```
from m5stack import lcd
lcd.setBrightness(0)   # 0-100
```
- 78 mA -> 23 mA com 'lcd.setBrightness(0)'' e 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
from m5stack import lcd
lcd.setBrightness(0)   # 0-100
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```
- 78 mA -> 23 mA com 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```



## 3. M5Stack Core (Basic/PSRAM MPU9250) com firmware MicroPython ESP32 Lobo + repositório tuupola :

(FAZER) Documentar melhor, mais exemplos, etc.

Reconhece PSRAM e MPU9250.

### Instalando firmware :

Apagando flash, ligar pressionando o 3o botão (C) para entrar em modo 'flash' :  
`$ pip install esptool`
`$ esptool.py -p /dev/ttyUSB0 -b 115200 --chip esp32 erase_flash`  
Instalando firmware ESP32 Lobo via terminal, usando repositório [MicroPython_ESP32_psRAM_LoBo-master/MicroPython_BUILD/firmware/esp32_all](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/tree/master/MicroPython_BUILD/firmware/esp32_all) :  
`$ ./flash`

Fazer download do [repositório do tuupola](https://github.com/tuupola/micropython-m5stack).  
Editar "Makefile" colocando "/dev/ttyUSB0" na 2a linha, "PORT := /dev/ttyUSB0". E retirar "--verbose" da linha 19 (comando 'sync').  
Editar "firmware/main.py" comentando linhas 47-48, 90-94 para desabilitar leitura de bateria (incompatível com M5Stack Basic/Gray).  
Rodar para copiar os arquivos do tuupola para '/flash' do M5Stack :  
`[roberto@nautilusvi micropython-m5stack-master]$ make sync`

### Testes e exemplos :

- 'Ctrl+D' mostra muitas mensagens de boot do MicroPython ESP32 Lobo

- com 'boot.py' e 'main.py' de 'MicroPython ESP32_LoBo_v3.2.24' + tuupola :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython ESP32_LoBo_v3.2.24 - 2018-09-06 on ESP32 board with ESP32
import gc
gc.collect()
gc.mem_free()    # 60560
```

- ver versão de MicroPython (após boot/reset também mostra) :
```
import os
os.uname()   # (sysname='esp32_LoBo', nodename='esp32_LoBo', release='3.2.24', version='ESP32_LoBo_v3.2.24 on 2018-09-06', machine='ESP32 board with ESP32')
```

- continuar... :
```
tft.clear()  # limpa a tela
tft.line(0, 0, 319, 239, 0x00FF00)   # linha verde na diagonal
```



## 4. MicroPython v1.12 ou mais recente, oficial

Vide [documentação oficial de MicroPython para ESP32](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html).

Não reconhece a tela e MPU9250, mas reconhece PSRAM e é firmware [MicroPython oficial mais recente, v1.12 de 12/2019](http://micropython.org/download)).  
Tem poucos drivers (apa106, ds18x20 e neopixel), então tem que instalar manualmente drivers de terceiros para a tela 
ILI9342C, MPU9250 e os vários dispositivos em Units, etc.

### 4.1 Instalando firmware :

Apagando flash, ligar pressionando o 3o botão (C) para entrar em modo 'flash' :  
`$ pip install esptool`
`$ esptool.py -p /dev/ttyUSB0 -b 115200 --chip esp32 erase_flash`  
Instalando firmware [MicroPython v1.12 ou mais recente, oficial](http://micropython.org/download), para M5Stack Basic, sem PSRAM :  
`$ esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash --flash_mode dio -z 0x1000 esp32-idf3-20191220-v1.12.bin`  
para M5Stack Gray, com PSRAM :  
`$ esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash --flash_mode dio -z 0x1000 esp32spiram-idf3-20191220-v1.12.bin`

### 4.2 Testes e exemplos :

- tem só 1 arquivo em '/pyboard', 'boot.py' (vazio, só com comentários e comandos que podem ser habilitados para webrepl, etc), que 
pode ser copiado usando 'rshell' (mais fácil) ou 'ampy' :
```
$ rshell -p /dev/ttyUSB0
> > ls -l /pyboard
   139 Jan  1 2000  boot.py
> cat /pyboard/boot.py
# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
#import webrepl
#webrepl.start()
```

- dando reset apertando uma vez o botão lateral vermelho no M5Stack Basic sem PSRAM :
```
I (457) cpu_start: Pro cpu up.
I (457) cpu_start: Application information:
I (457) cpu_start: Compile time:     Dec 20 2019 07:50:41
I (460) cpu_start: ELF file SHA256:  0000000000000000...
I (466) cpu_start: ESP-IDF:          v3.3
I (471) cpu_start: Starting app cpu, entry point is 0x40083600
I (0) cpu_start: App cpu up.
I (482) heap_init: Initializing. RAM available for dynamic allocation:
I (488) heap_init: At 3FFAE6E0 len 00001920 (6 KiB): DRAM
I (494) heap_init: At 3FFBA488 len 00025B78 (150 KiB): DRAM
I (501) heap_init: At 3FFE0440 len 00003AE0 (14 KiB): D/IRAM
I (507) heap_init: At 3FFE4350 len 0001BCB0 (111 KiB): D/IRAM
I (513) heap_init: At 40092D6C len 0000D294 (52 KiB): IRAM
I (520) cpu_start: Pro cpu start user code
I (202) cpu_start: Chip Revision: 1
W (203) cpu_start: Chip revision is higher than the one configured in menuconfig. Suggest to upgrade it.
I (206) cpu_start: Starting scheduler on PRO CPU.
I (0) cpu_start: Starting scheduler on APP CPU.
MicroPython v1.12 on 2019-12-20; ESP32 module with ESP32
Type "help()" for more information.
```
no M5Stack Gray com PSRAM :
```
I (525) psram: This chip is ESP32-D0WD
I (525) spiram: Found 32MBit SPI RAM device
I (525) spiram: SPI RAM mode: flash 40m sram 40m
I (528) spiram: PSRAM initialized, cache is in low/high (2-core) mode.
I (535) cpu_start: Pro cpu up.
I (539) cpu_start: Application information:
I (544) cpu_start: Compile time:     Dec 20 2019 07:52:43
I (550) cpu_start: ELF file SHA256:  0000000000000000...
I (556) cpu_start: ESP-IDF:          v3.3
I (561) cpu_start: Starting app cpu, entry point is 0x40083d90
I (0) cpu_start: App cpu up.
I (1446) spiram: SPI SRAM memory test OK
I (1447) heap_init: Initializing. RAM available for dynamic allocation:
I (1447) heap_init: At 3FFAE6E0 len 00001920 (6 KiB): DRAM
I (1453) heap_init: At 3FFBA658 len 000259A8 (150 KiB): DRAM
I (1459) heap_init: At 3FFE0440 len 00003AE0 (14 KiB): D/IRAM
I (1466) heap_init: At 3FFE4350 len 0001BCB0 (111 KiB): D/IRAM
I (1472) heap_init: At 400973F0 len 00008C10 (35 KiB): IRAM
I (1479) cpu_start: Pro cpu start user code
I (155) cpu_start: Chip Revision: 1
W (156) cpu_start: Chip revision is higher than the one configured in menuconfig. Suggest to upgrade it.
I (159) cpu_start: Starting scheduler on PRO CPU.
I (0) cpu_start: Starting scheduler on APP CPU.
MicroPython v1.12 on 2019-12-20; ESP32 module (spiram) with ESP32
Type "help()" for more information.
>>>
```

- com 'boot.py' de 'MicroPython v1.12 on 2019-12-20' no M5Stack Basic sem PSRAM :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.12 on 2019-12-20; ESP32 module with ESP32
Type "help()" for more information.
import gc
gc.collect()
gc.mem_free()
    114368   #  111.7 kB
```
no M5Stack Gray com PSRAM :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.12 on 2019-12-20; ESP32 module (spiram) with ESP32
Type "help()" for more information.
import gc
gc.collect()
gc.mem_free()
    4093632   #  3998 kB
```

- ver versão de MicroPython (após boot/reset também mostra) :
```
import os
os.uname()
    (sysname='esp32', nodename='esp32', release='1.12.0', version='v1.12 on 2019-12-20', machine='ESP32 module (spiram) with ESP32')
```

- [módulo 'machine'](https://docs.micropython.org/en/latest/library/machine.html) e frequência do ESP32 (default é 240 MHz, 
pode ser alterada para 20MHz, 40MHz, 80Mhz, 160MHz or 240MHz) :
```
import machine
machine.freq()
    160000000
machine.freq(240000000)
    I (149260) pm_esp32: Frequency switching config: CPU_MAX: 240, APB_MAX: 240, APB_MIN: 240, Light sleep: DISABLED
machine.freq(80000000)
    I (301440) pm_esp32: Frequency switching config: CPU_MAX: 80, APB_MAX: 80, APB_MIN: 80, Light sleep: DISABLED
>>> machine.freq(40000000)
    I (504150) pm_esp32: Frequency switching config: CPU_MAX: 40, APB_MAX: 40, APB_MIN: 40, Light sleep: DISABLED
machine.freq(20000000)
    I (500920) pm_esp32: Frequency switching config: CPU_MAX: 20, APB_MAX: 20, APB_MIN: 20, Light sleep: DISABLED
```

- help :
```
help()
    Welcome to MicroPython on the ESP32!
    
    For generic online docs please visit http://docs.micropython.org/
    
    For access to the hardware use the 'machine' module:
    
    import machine
    pin12 = machine.Pin(12, machine.Pin.OUT)
    pin12.value(1)
    pin13 = machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP)
    print(pin13.value())
    i2c = machine.I2C(scl=machine.Pin(21), sda=machine.Pin(22))
    i2c.scan()
    i2c.writeto(addr, b'1234')
    i2c.readfrom(addr, 4)
    
    Basic WiFi configuration:
    
    import network
    sta_if = network.WLAN(network.STA_IF); sta_if.active(True)
    sta_if.scan()                             # Scan for available access points
    sta_if.connect("<AP_name>", "<password>") # Connect to an AP
    sta_if.isconnected()                      # Check for successful connection
    
    Control commands:
      CTRL-A        -- on a blank line, enter raw REPL mode
      CTRL-B        -- on a blank line, enter normal REPL mode
      CTRL-C        -- interrupt a running program
      CTRL-D        -- on a blank line, do a soft reset of the board
      CTRL-E        -- on a blank line, enter paste mode
    
    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (drivers, etc, do UIFlow MicroPython firmware) :

```
help('modules')
    __main__          framebuf          ucryptolib        urandom
    _boot             gc                uctypes           ure
    _onewire          inisetup          uerrno            urequests
    _thread           machine           uhashlib          uselect
    _webrepl          math              uhashlib          usocket
    apa106            micropython       uheapq            ussl
    btree             neopixel          uio               ustruct
    builtins          network           ujson             utime
    cmath             ntptime           umqtt/robust      utimeq
    dht               onewire           umqtt/simple      uwebsocket
    ds18x20           sys               uos               uzlib
    esp               uarray            upip              webrepl
    esp32             ubinascii         upip_utarfile     webrepl_setup
    flashbdev         ucollections      upysh             websocket_helper
    Plus any modules on the filesystem
```

- [módulo 'esp'](https://docs.micropython.org/en/latest/library/esp.html) e tamanho da memória flash interna (4 MB) :
```
import esp
esp.
    __class__       __name__        LOG_DEBUG       LOG_ERROR
    LOG_INFO        LOG_NONE        LOG_VERBOSE     LOG_WARNING
    dht_readinto    flash_erase     flash_read      flash_size
    flash_user_start                flash_write     gpio_matrix_in
    gpio_matrix_out                 neopixel_write  osdebug
esp.flash_size()
    4194304
```

- [módulo 'esp32'](https://docs.micropython.org/en/latest/library/esp32.html), temperatura e leitura do sensor Hall do ESP32 :
```
import esp32
esp32.
    __class__       __name__        Partition       RMT
    ULP             WAKEUP_ALL_LOW  WAKEUP_ANY_HIGH
    hall_sensor     raw_temperature                 wake_on_ext0
    wake_on_ext1    wake_on_touch
esp32.raw_temperature()   # in F
    127
(esp32.raw_temperature() - 32)/1.8   # in C
    52.77778
esp32.hall_sensor()
    119
```

- inicializa I2C via ['machine.I2C'](https://docs.micropython.org/en/latest/esp32/quickref.html#i2c-bus), forma padrão do MicroPython para ESP32 :
```
from machine import I2C, Pin
i2c = I2C(freq=400000, scl=Pin(22), sda=Pin(21))
i2c.scan()
    []        # on M5Stack Basic
    [104]     # on M5Stack Gray 4MB PSRAM with MPU9250 the default I2C address of MPU9250 is 0x68 = 104
```

- (FAZER) exemplo de uso de tela com drivers externos;

- (FAZER) uso de botões A, B e C vi leitura de GPIO :

- (FAZER) uso de speaker via PWN/GPIO :

- (FAZER) uso de MPU9250 com [driver externo :
    - 'MicroPython I2C driver for MPU9250 9-axis motion tracking device' do tuupola](https://github.com/tuupola/micropython-mpu9250);
    - ['Drivers for InvenSense inertial measurement units MPU9250, MPU9150, MPU6050' de micropython-IMU](https://github.com/micropython-IMU/micropython-mpu9x50)


### 4.3 Consumo de energia

#### 4.3.1 Medição de energia do 'M5Stack Basic' usando UM25C, sem M5Stack Core Bottom (pois pode estar carregando a bateria) :
- com 'boot.py' e 'main.py' originais, ou com 'boot.py' simplificado, i = 58 mA;
- 60 mA -> 39 mA com 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```

#### 4.3.2 Medição de energia do 'M5Stack Gray 4MB PSRAM with MPU9250' usando UM25C, sem M5Stack Core Bottom (pois pode estar carregando a bateria) :
- com 'boot.py' original, i = 40 mA;
- 40 mA -> 23 mA com 'machine.deepsleep(10000)'', reinicializa em 10s :
```
import machine
machine.deepsleep(10000)   # a tela é desligada, depois acorda em 10s 
```