# ADC_loop_Pycom_Chrono.py
# Reads the xxPy (LoPy/LoPy4/etc) ADC and measures the time (using 'Timer.Chrono()') and speed to read samples.
import machine

def ADCloopBenchmark(nsamples=20000, nbits=12):
    adc = machine.ADC(0, bits=nbits)
    adcread = adc.channel(pin='P13')
    chrono = machine.Timer.Chrono()
    chrono.start()
    for i in range(nsamples):
        val = adcread()
    dt = chrono.read_us()
    print("%u ADC readings done after %5.3f ms" % (nsamples, dt/1e3))
    print("Mean time for each ADC reading = %4.2f us" % (dt/nsamples))
    print("ADC reading = %5.3f ksamples/s" % (1e3*nsamples/dt))

