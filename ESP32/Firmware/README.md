# Firmwares for MicroPython on [ESP32](http://esp32.net/)

Look at the [MicroPython Download -> Firmware for Generic ESP32 module site](https://micropython.org/download/esp32/) and [MicroPython Download -> Firmware for TinyPICO site](https://micropython.org/download/tinypico/) for official firmwares for the most popular ESP32 boards. But firmwares with double precision float point numbers (DP/FP64) are not provided.

All MicroPython firmwares here were compiled from the [MicroPython source code for ESP32](hhttps://github.com/micropython/micropython/tree/master/ports/esp32), are not official and don't have any warranty, but all steps to build are open and can be reproduced.

## Firmware features

The MicroPython firmwares here are named in the form :  
```<ESP32 board name>_<'idf3'/'idf4'>_<optional 'ulab_'><'sp' or 'dp'>_<'thread'>_<version>_<date>.dfu``` 
where :
- [ESP32 board](http://esp32.net/) name can be :
  - `esp32`, for the common ESP32 board, with 520 kB of internal RAM and 4 MB of QSPI flash;
  - `esp32-d2wd`, for the boards with ESP32 D2WD, with 520 kB of internal RAM and 2 MB of embedded QSPI flash;
  -  `esp32-spiram`, for the ESP32 boards with 4 MB of external PSRAM/SPIRAM (using QSPI bus) and 4 MB of QSPI flash;
  - `tinypico`, for the [TinyPico ESP32 boards](https://www.tinypico.com/) with 4 MB of external PSRAM/SPIRAM (using QSPI bus) and 4 MB of QSPI flash;
- 'idf3' means firmware built with [ESP-IDF](https://github.com/espressif/esp-idf) v3.3, with support for BLE, LAN and PPP. 'idf4' means firmware built with [ESP-IDF](https://github.com/espressif/esp-idf) v4.0, with support for BLE, but no LAN or PPP;
- 'ulab' means the [ulab native (in C) module](https://github.com/v923z/micropython-ulab), a NumPy-like array manipulation library, is included in the firmware;
- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html) and allowing multithreading, and is always used by all ESP32 MicroPython firmwares, here and the official ones;

For example : 
```esp32-spiram_idf3_ulab_dp_thread_v1.12-663-gea4670d5a_2020-07-29.bin```  
means it is a v1.12-663-gea4670d5 firmware, from July 29 2020, built with ESP-IDF v3.3, with ulab module included, double precision float point numbers and threads enabled, for ESP32 generic board with 4 MB PSRAM (SPIRAM) and 4 MB of flash memory.


## Flashing firmware on ESP32

For flashing firmware on ESP32, the ['esptool software'](https://github.com/espressif/esptool) should be used, see the :
- [MicroPython documentation for ESP32, section "Getting started with MicroPython on the ESP32 / Deploying the firmware"](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html#deploying-the-firmware);
- ["MicroPython Downloads / Firmware for Generic ESP32 module" site](http://micropython.org/download/esp32/) or ["MicroPython Downloads / Firmware for TinyPICO" site](http://micropython.org/download/tinypico/).
